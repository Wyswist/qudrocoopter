\documentclass[10pt,journal, a4paper]{IEEEtran}
\usepackage[MeX]{polski}
\usepackage[utf8]{inputenc}  % Polskie
\usepackage[polish]{babel}     %
\usepackage{parskip}
\usepackage{latexsym}
\usepackage{graphicx}
\usepackage{url}
\usepackage{verbatim}
\usepackage{epstopdf}
\usepackage{cite}
\usepackage{ragged2e}
\usepackage{comment}
\usepackage{caption}
\setlength{\parindent}{1em}
\author{Andrzej Tkaczyk \\Bartłomiej Piotrowski \\ Krzysztof Łebek}
\title{Sterowanie silnikami quadrocoptera}
\begin{document}
\maketitle
\begin{abstract}
 W artykule tym przedstawione zostały najważniejsze zagadnienia związane z sterowaniem silniakmi quadrocoptera. Przedstawionon proces modelowania silników do qudrocoptera napędzanego silnikami DC. W poniższym artykule zaprezentowano opis w postaci równań dynamiki ruchu robota. Model oraz jego symulacja zostały sporządzone w środowisku MatLab oraz simulink
\end{abstract}
\begin{IEEEkeywords} %key words
Qudrocopter, silniki DC, modelowanie, symulacja, regulator.
\end{IEEEkeywords}
\section{Wstęp}
\label{rozdzial:Wstep}

BSP - Bezzałogowe Statki Powietrzne to statki powietrzne które nie wymagają załogi na pokładzie statku aby móc szybować. Głównym zastosowaniem BSP jest wojsko, a konkretniej wykorzystywane są: przez siły zbrojne do obserwacji i rozpoznania przez co zwykle wyposażone są w osprzęt służący do obserwacji w postaci głowic optoelektronicznych. Uzbrojone i przeznaczone do wykonywania działań bojowych statki są określane jako Unmanned combat air vehicle (UCAV)[1].

W ciągu ostatnich lat, BSP znajdują coraz szersze zainteresowanie wśród przeciętnych obywateli, nie zajmujących się militariami. Są to głównie małe modele, potocznie zwane dronami, używane w celach komercyjnych (wymagające licencji) oraz w celach rozrywkowych (nie wymagające). W poniższej pracy przedstawimy wraz z współautorami model symulacyjny takiego drona.

\section{Podział dronów}
\label{rozdzial:Podzial_dronow}

Drony można sklasyfikować na kilka kategorii w zależności od przyjętego kryterium: 
\subsection{Klasyfikacja według masy}
Bardzo ciężkie - masa > 2000 kg

Ciężkie - 2000 kg > masa > 200 kg

Średnie - 200 kg > masa > 50 kg

Lekkie - 50 kg > masa > 5 kg

Bardzo lekkie - 5 kg > masa

\subsection{Klasyfikacja według zastosowania}

Drony do użytku wojskowego.
\begin{figure}[h]
\centering
\includegraphics[width=7cm]{wojskowy}
\caption{Wojskowy dron}
\end{figure}

Drony do użytku prywatnego. 
\begin{figure}[h]
\centering
\includegraphics[width=7cm]{mavic_pro}
\caption{Dron do użytku prywatnego}
\end{figure}

Istnieje jeszcze wiele innych podziałów, ale nie to jest przedmiotem tego artykułu.
\section{Model symulacyjny}
\label{rozdzial:Model_symulacyjny}

Model symulacyjny wykonany został w środowisku Matlab
Simulink, pozwalający na stosunkowo prostą symulację na podstawie równań matematycznych opisujących fizykę zjawiska.

Tu by się przydał cały schemat modelu.
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
%\includegraphics[width=2.5in]{05_underwater}
\caption{Pełny model układu.}
\label{fig:05_underwater}
\end{figure}


\section{Silnik DC}
\label{rozdzial:Dynamika}
Nie cackać się tylko wpierdolić model silnika DC który był na MUMach. Opisać zasadę działania, itp itd tak jak to robili chłopaki z pompą insulinową.

Na sam koniec tej sekcji opisać, że sterowanie silnikami DC z wykorzystaniem regulatorów PID.
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
%\includegraphics[width=2.5in]{08_dynamics}
\caption{Model robota, który zostanie poddany modelowaniu.}
\label{fig:08_dynamics}
\end{figure}
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
U_1&{}={}&0, \label{eq:energy_1}\\
U_2&{}={}&-M_2 ge\cos(\theta_1+\theta_2), \\
K_1&{}={}&\frac{1}{2}M_1 (r\omega_1)^2, \\
K_2&{}={}&\frac{1}{2}M_2\Big( (r\omega_1 - e\cos(\theta_1+\theta_2)(\omega_1+\omega_2))^2\nonumber\\
&&+ (e\sin(\theta_1+\theta_2)(\omega_1+\omega_2))^2 \Big), \\
T_1&{}={}&\frac{1}{2}J_1\omega_1^2, \\
T_2&{}={}&\frac{1}{2}J_2(\omega_1+\omega_2)^2\label{eq:energy_2}.
\end{eqnarray}
\setlength{\arraycolsep}{5pt}
gdzie:\\
$U_1$ - energia potencjalna kuli w odniesieniu do jej środka, $U_2$ - energia potencjalna wahadła w odniesieniu do środka kuli, $K_1$ - energia kinetyczna kuli, $K_2$ - energia kinetyczna wahadła, $T_1$ - energia rotacji kuli, $T_2$ - energia rotacji wahadła, $r$ - promień kuli, $e$ - odległość między środkiem kuli a końcem wahadła, $\theta_1$ - kąt obrotu kuli, $\theta_2$ - kąt obrotu wahadła w odniesieniu do kuli, $\omega_1$ - prędkość kątowa kuli, $\omega_2$ - prędkość kątowa wahadła w odniesieniu do kuli, $J_1$ - moment bezwładności kuli względem jej środka, $J_2$ - moment bezwładności wahadła względem środka kuli, $M_1$ - masa kuli, $M_2$ - masa wahadła, $g$ - przyspieszenie ziemskie. 

Równanie Lagrange'a ma następującą postać:
\begin{equation}
L=K_1+K_2+T_1+T_2-U_1-U_2.
\end{equation}
Równania ruchu mają postać:
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
\frac{d}{dt}\Big( \frac{\partial L}{\partial\omega_1} \Big)-\frac{\partial L}{\partial\theta_1}&{}={}&-T+T_f, \label{eq:lagrange_1}\\
\frac{d}{dt}\Big( \frac{\partial L}{\partial\omega_2} \Big)-\frac{\partial L}{\partial\theta_2}&{}={}&T\label{eq:lagrange_2}.
\end{eqnarray}
\setlength{\arraycolsep}{5pt}
gdzie:\\
$T$ - moment siły pomiędzy wahadłem, a kulą, \\
$T_f$ - moment pochodzący od siły tarcia: \\
\begin{equation}
T_f = T_c+T_v\omega_1,\label{eq:torque}
\end{equation}
gdzie: $T_c$ - współczynnik tarcia Coulombowskiego, $T_v$ - współczynnik tarcia wiskotycznego.\\
Wstawiając (\ref{eq:energy_1}-\ref{eq:energy_2}) do (\ref{eq:lagrange_1}) i (\ref{eq:lagrange_2}) mamy:
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
-T+T_f&{}={}&a_1(J_1+J_2+M_1r^2+M_2r^2\nonumber\\
&&+M_2e^2-2M_2re\cos(\theta_1+\theta_2))\nonumber\\
&& +a_2(J_2-M_2re\cos(\theta_1+\theta_2)+M_2e^2)\nonumber\\
&& +M_2re\sin(\theta_1+\theta_2)(\omega_1+\omega_2)^2 \nonumber\\
&&+ M_2ge\sin(\theta_1+\theta_2)\label{eq:lagrange_second_1}, \\
T&{}={}&a_1(J_2-M_2re\cos(\theta_1+\theta_2)+M_2e^2)\nonumber\\
&&+a_2(J_2+M_2e^2)\nonumber\\
&&+M_2ge\sin(\theta_1+\theta_2)\label{eq:lagrange_second_2}.
\end{eqnarray}
\setlength{\arraycolsep}{5pt}

Należy teraz wprowadzić do układu urządzenie pobudzające wahadło. W tym celu zamodelowano silnik DC o~następujących równaniach:
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
V&{}={}&I_t R_t+L_t \frac{d}{dt}I_t+c_v\omega, \label{eq:motor_voltage}\\
M_{em}-M_{obc}&{}={}&J_c \frac{d}{dt}\omega \label{eq:motor_torque}\\
M_{em}&{}={}&c_M I_t \label{eq:motor_noise},
\end{eqnarray}
\setlength{\arraycolsep}{5pt}

\begin{figure*}[!ht]
\captionsetup{justification=centering}
\centering
%\includegraphics[width=\textwidth]{11_model_simulink_1}
%\caption{Model robota kulistego}
%\label{fig:11_model_simulink}
\end{figure*}
gdzie:\\
$V$ - napięcie przyłożone do zacisków silnika, $I_t$ - płynący przez silnik prąd, $R_t$ - rezystancja twornika silnika, $C_v$ - stała prędkościowa silnika, $\omega$ - prędkość obrotowa silnika, $M_{em}$ - moment wytwarzany przez silnik, $C_M$ - stała momentowa silnika, $M_{obc}$ - moment obciążenia, $J_c$ - moment bezwładności silnika.



Następnie należy zlinearyzować równania (\ref{eq:lagrange_second_1}) i (\ref{eq:lagrange_second_2}). Wartości wychylenia wahadła i kuli są niewielkie, dlatego $\theta_1+\theta_2\ll1$ oraz $\omega_1+\omega_2\ll1$.  Równania (\ref{eq:lagrange_second_1}) i (\ref{eq:lagrange_second_2}) po linearyzacji prezentują się następująco:
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
-T+T_f&{}={}&a_1(J_1+J_2+M_1r^2+M_2r^2+M_2e^2\nonumber\\
&&-2M_2re)+a_2(J_2-M_2re+M_2e^2)\nonumber\\
&&+ M_2ge(\theta_1+\theta_2)\label{eq:lagrange_linear_1}, \\
T&{}={}&a_1(J_2-M_2re+M_2e^2)+a_2(J_2+M_2e^2)\nonumber\\
&&+M_2ge(\theta_1+\theta_2)\label{eq:lagrange_linear_2}.
\end{eqnarray}
\setlength{\arraycolsep}{5pt}

\section{Model robota}
\label{rozdzial:Model}


Model obiektu składać się będzie z dwóch części, modelu silnika DC oraz kuli z wahadłem. 
Model kuli z wahadłem otrzymujemy poprzez zastosowanie przekształcenia Laplace'a do równań (\ref{eq:lagrange_linear_1}) oraz (\ref{eq:lagrange_linear_2}), wyeliminowanie $T_f$ przy użyciu (\ref{eq:torque}) oraz pominięcie stałej $T_c$.
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
-T&{}={}&[-sT_v+egM_2+s^2{J_1+J_2+r^2M_1+(e-r)^2M_2}]\theta_1\nonumber\\
&&+[s^2J_2+e{g+(e-r)s^2}M_2]\theta_2 \label{eq:lagrange_1_s},\\
T&{}={}&s^2J_2(\theta_1 + \theta_2)+eM_2[{g+(e-r)s^2}\theta_1\nonumber \\
 &&+(g+es^2)\theta_2] \label{eq: lagrange_2_s}.
\end{eqnarray}
Poprzez eliminacje z równań (\ref{eq:lagrange_1}) oraz (\ref{eq: lagrange_2_s}) $T$ otrzymujemy transmitancje kuli z wahadłem:
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
\frac{\theta_1}{\theta_2}&{}={}&\frac{-2s^2J_2+e(-2g+(-2e+r)s^2)M_2}{-sT_v+2egM_2+s^2A} \label{eq:transfer_function}
\end{eqnarray}
gdzie:\\
$A=J_1+2J2+r^2M_1+(2e^2-3er+r^2)M_2$.\\
Model silnika DC otrzymujemy poprzez transformatę Laplace'a oraz przekształcenie równań (\ref{eq:motor_voltage}), (\ref{eq:motor_torque}) oraz ({\ref{eq:motor_noise}):
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
I_t(s)&{}={}&\frac{1}{R_t+sL_t}U_t(s)-C_v\omega(s) \label{eq:motor_voltage_s}, \\
M_{e}&{}={}&c_MI_t(s) \label{eq:motor_torgue_s}, \\
\omega(s)&{}={}&\frac{1}{sJ_c}(M_{em}(s)-M_{obc}(s)) \label{eq:motor_noise_s}.
\end{eqnarray}
Schemat blokowy modelu silnika przedstawiono na rysunku \ref{fig:10_schemat_blokowy}.
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
%\includegraphics[width=3.5in]{10_schemat_blokowy}
%\caption{Schemat blokowy modelu silnika DC.}
%\label{fig:10_schemat_blokowy}
\end{figure}
Na podstawie otrzymanych zależności stworzono model obiektu w programie Simulink. Jako moment obciążenia silnika przyjęto moment pochodzący od siły tarcia oraz od siły grawitacji działającej na wahadło. 
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
M_{obc}&{}={}&T_v\omega_1+egM_2sin(\theta_2) \label{eq:M_obc}
\end{eqnarray}
Transmitancja opisująca kulę z wahadłem została pomnożona przez promień kuli $r$, tak aby wielkością wyjściową było przemieszczenie robota. 
%tutututututututttttttttttttttttttttttttttttttttttttttttttttttt
\begin{figure*}[!ht]
\captionsetup{justification=centering}
\centering
%\includegraphics[width=\textwidth]{13_model_reg}
\caption{Model układu regulacji robota kulistego z możliwością zadania wymuszenia skokowego lub wymuszenia liniowo narastającego.}
\label{fig:13_model_reg}
\end{figure*}
Podczas modelowania przyjęto następujące parametry obiektu: \\
$R_t=0,29\ \Omega$, $C_v=0,253\ \frac{Vs}{rad}$, $C_M=0,235\ \frac{Nm}{A}$, $J_c=0,0014\ kgm^2$, $r=0,226\ m$, $e=0,065\ m$, $J_1=0,0633\ kgm^2$, $J_2=0,007428\ kgm^2$, $M_1=3294\ g$, $M_2=1795\ g$, $g=9,81\ \frac{m}{s^2}$, $T_v = -0,1398\ \frac{Nm\cdot s}{rad}$.

Podczas symulacji działania obiektu zbadano odpowiedź układu na skokową oraz impulsową wartość zadaną.  
\begin{figure}[!ht]
%\captionsetup{justification=centering}
%\centering
%\includegraphics[width=0.48\textwidth]{12_odpowiedz_2}
%\caption{Odpowiedź obiektu na wymuszenie skokowe o amplitudzie 3.}
%\label{fig:12_odpowiedz_2}
\end{figure}

\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
%\includegraphics[width=0.48\textwidth]{09_odpowiedz}
\caption{Odpowiedź obiektu na wymuszenie impulsowe o amplitudzie 3 i czasie trwania impulsu 1s.}
\label{fig:09_odpowiedz}
\end{figure}
Zgodnie z przewidywaniami odpowiedź na impulsową wartość zadaną ma postać gasnących oscylacji (rysunek \ref{fig:09_odpowiedz}), natomiast na skokowe wymuszenie jest funkcją narastającą (zbliżoną do funkcji liniowej)(rysunek \ref{fig:12_odpowiedz_2}).

\section{Układ regulacji}

Sterowanie przemieszczeniem liniowym robota odbywać się będzie poprzez zmianę napięcia zasilania silnika DC znajdującego się wewnątrz kuli.

W celu zapewnienia możliwości wygodnego sterowania modelowanym robotem konieczne było zaimplementowanie układu regulacji robota kulistego. 
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
%\includegraphics[width=0.48\textwidth]{16_jakies_gowno}
\caption{Odpowiedź układu z regulatorem PID na wymuszenie skokowe o amplitudzie 3.}
\label{fig:16_jakies_gowno}
\end{figure}


Początkowo zbadano układ z regulatorem PID (gdzie: $K_p = 0,7 $, $K_i=0,574 $, $K_d=0,5 $) (rysunek \ref{fig:16_jakies_gowno}), jednak nie dawał on oczekiwanych rezultatów. Dlatego zdecydowano się na wykorzystanie regulatora zmiennych stanu.

Stanami układu będą położenie kątowe i prędkość kuli, a także jej przyspieszenie. Drugi i trzeci stan będą wyznaczane poprzez różniczkowanie kolejno jednokrotne i dwukrotne kąta wychylenia kuli będącego stanem pierwszym.

Parametrami zbudowanego regulatora (rysunek \ref{fig:13_model_reg}) są wzmocnienia poszczególnych stanów. Nastawy zostały dobrane doświadczalnie i wyniosły one $K_\alpha = 1,083$, $K_\omega = 0,48$ oraz $K_\epsilon = 0,5\cdot10^{-15}$.
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
%\includegraphics[width=0.48\textwidth]{14_odp_skok_reg_2}
\caption{Odpowiedź układu z regulatorem zmiennych stanu na wymuszenie skokowe o amplitudzie 3.}
\label{fig:14_odp_skok_reg_1}
\end{figure}
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
%\includegraphics[width=0.48\textwidth]{15_odp_zbocze_reg_2}
\caption{Odpowiedź układu z regulatorem zmiennych stanu na wymuszenie liniowo narastające.}
\label{fig:15_odp_zbocze_reg_1}
\end{figure}

Na podstawie uzyskanych przebiegów dla wymuszenia skokowego (rysunek \ref{fig:14_odp_skok_reg_1}) oraz dla wymuszenia liniowo narastającego (rysunek \ref{fig:15_odp_zbocze_reg_1}) można stwierdzić, że regulator działa w sposób zbliżony do poprawnego. Jednak w obu tych przebiegach występują odchyłki statyczne. Podjęto próbę ich wyeliminowania poprzez dodanie akcji całkującej na wyjściu z regulatora zmiennych stanu(rysunek \ref{fig:17_jakies_gowno}).
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
%\includegraphics[width=0.48\textwidth]{17_jakies_gowno}
\caption{Odpowiedź układu z regulatorem zmiennych stanu z akcją całkującą na wymuszenie skokowe o amplitudzie 3.}
\label{fig:17_jakies_gowno}
\end{figure}

\section{Wnioski}


Regulator PID pozwolił uzyskać zerową odchyłkę statyczną, jednak wystąpiło relatywnie duże przeregulowanie oraz długi czas regulacji ($24,9\ s$). Przejście na regulator zmiennych stanu  spowodowało znaczne skrócenie czasu regulacji($5,48\ s$), zmniejszenie przeregulowania. Niestety nie udało się zachować zerowej wartości odchyłki statycznej. Dodanie akcji całkującej pozwoliło zniwelować odchyłkę statyczną do zera, jednak spowodowało to znaczne wydłużenie czasu regulacji i zwiększenie przeregulowania. 

Na podstawie wykonanych badań uznano, że regulator zmiennych stanu bez akcji całkującej jest najlepszym rozwiązaniem w danych układzie. Jako kryterium wyboru przyjęto następujące wskaźniki jakości: czas regulacji oraz przeregulowanie. 


Jednak dla wymuszeń o wartościach większych niż 10, układ wchodził w bardzo duże oscylacje, a dobrane nastawy przestawały mieć sens. Być może inne wartości nastaw poprawiłby jego działanie, jednak ich dobór musiałby zostać wykonany inną metodą niż metoda doświadczalna.

\bibliographystyle{IEEEtran}
\bibliography{IEEEabrv,mybibfile}

\end{document}