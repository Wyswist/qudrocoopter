PROJECT=QudroCopter
PROJECT_VIEW=${PROJECT}_view
#OUT nie może nazywać się tak samo jak jakiś z targetów np. build
OUT=_build
# OUT=.
TMP=tmp
LATEX=pdflatex
# LATEX=pdflatex

all: clean build

build: ${OUT} latex1 bibligrafia latex2 latex3 compress view

latex1:
	${LATEX} --output-directory=${OUT} ${PROJECT}

latex2:
	${LATEX} --output-directory=${OUT} ${PROJECT}

latex3:
	${LATEX} --output-directory=${OUT} ${PROJECT}
	
bibligrafia:
	bibtex ${OUT}/${PROJECT}

pdf:
	# "D:/Programy/Foxit Reader/Foxit Reader.exe" ${OUT}/${PROJECT}.pdf
	okular ${OUT}/${PROJECT}.pdf

${OUT}:
	mkdir ${OUT}

compress:
	cd ${OUT} && \
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=${TMP}.pdf ${PROJECT}.pdf && \
	rm -f ${PROJECT}.pdf && \
	mv ${TMP}.pdf ${PROJECT}.pdf

view:
	cp ${OUT}/${PROJECT}.pdf ${OUT}/${PROJECT_VIEW}.pdf

clean:
	rm -f ${OUT}/*.aux
	rm -f ${OUT}/*.bbl
	rm -f ${OUT}/*.bcf
	rm -f ${OUT}/*.blg
	rm -f ${OUT}/*.bbl
	rm -f ${OUT}/${PROJECT}.pdf
	rm -f ${OUT}/*.run.xml
	rm -f ${OUT}/*.lof
	rm -f ${OUT}/*.lot
	rm -f ${OUT}/*.out
	rm -f ${OUT}/*.toc

# pdflatex --output-directory=build inzynierka
